package org.kamegatze.parsing_json_opcodes_in_asm_instractions;

import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.Instructions;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.operand_serialization.Operands;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.Key;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.KeyOpcode;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.immediate.Immediate;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.parser_json.Parser;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.parser_json.ParserInstructions;

import java.io.IOException;
import java.math.BigInteger;
import java.util.*;

public class Main {

    public static byte[] decodeHexString(String hexString) {
        return HexFormat.of().parseHex(hexString);
    }


    public static void main(String[] args) throws IOException {
        /*
        * create parser
        * */
        Parser parser = new ParserInstructions();

        /*
        * get elements from json
        * */
        Instructions instructions = (Instructions) parser.getRootJson();

        /*
        * transformations elements from json to current in view of
        * */
        Map<Key, Map<String, List<Object>>> asmInstruction = parser.transformation(instructions);

        Key keyOpcode = KeyOpcode.builder()
                .opcode(new BigInteger(decodeHexString("14")))
                .immediate(Immediate.builder()
                        .size(1)
                        .value("#1")
                        .build())
                .build();

        Map<String, List<Object>> arguments = asmInstruction.get(keyOpcode);

        /*
        * for example get operands by opcodes
        * */
        List<Operands> operands = arguments.get("operands")
                .stream().map(item -> (Operands) item ).toList();

        /*
        * for example get asm instruction by opcodes
        * */
        String instruction = (String) arguments.get("asmInstruction").get(0);

        System.out.println(instruction);
        System.out.println(operands);
    }
}