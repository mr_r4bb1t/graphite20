package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.item_serialization.Item;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

@Data
public class Instructions implements RootJson{

    @JsonProperty
    private String instruction_set;

    @JsonProperty
    private Map<String, Item> instructions;

    private String path;

    private Instructions() {
    }

    @Builder(builderMethodName = "builder")
    public static Instructions builderInstruction() throws IOException {
        Properties properties = new Properties();

        properties.load(new FileInputStream("./src/main/resources/app.properties"));

        String path = properties.getProperty("path.jsonFile");

        Instructions instructions = new Instructions();

        instructions.setPath(path);



        return instructions;
    }

    @Override
    public String toString() {
        return "Instructions{" +
                "instruction_set='" + instruction_set + '\'' +
                ", instructions=" + instructions +
                '}';
    }

    @Override
    public RootJson getRooJson(ObjectMapper objectMapper) throws IOException {

        File file = new File(path);

        return objectMapper.readValue(file, Instructions.class);
    }
}
