package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public interface RootJson {

    RootJson getRooJson(ObjectMapper objectMapper) throws IOException;
}
