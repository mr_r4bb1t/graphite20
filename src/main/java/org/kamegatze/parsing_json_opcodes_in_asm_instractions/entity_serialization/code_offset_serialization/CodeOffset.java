package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.code_offset_serialization;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CodeOffset {
    @JsonProperty
    private Integer size;

    @JsonProperty
    private String value;
}
