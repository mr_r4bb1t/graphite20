package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.data_offset_serialization;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DataOffset {
    @JsonProperty
    private Integer size;

    @JsonProperty
    private String value;

    @Override
    public String toString() {
        return "DataOffset{" +
                "size=" + size +
                ", value='" + value + '\'' +
                '}';
    }
}
