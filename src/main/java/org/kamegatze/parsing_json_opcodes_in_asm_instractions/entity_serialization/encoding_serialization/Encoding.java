package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.encoding_serialization;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.code_offset_serialization.CodeOffset;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.data_offset_serialization.DataOffset;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.evex_serialization.EVEXPrefix;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.immediate_serializtion.Immediate;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.mod_rm_serializtion.ModRM;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.prefix_serializtion.Prefix;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.vex_serializtion.VEXPrefix;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.rex_serializtion.REXPrefix;

import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Encoding {

    @JsonProperty
    private REXPrefix REX;

    @JsonProperty
    private Map<String, String> opcode;

    @JsonProperty
    private ModRM ModRM;

    @JsonProperty
    private VEXPrefix VEX;

    @JsonProperty
    private EVEXPrefix EVEX;

    @JsonProperty
    private Immediate immediate;

    @JsonProperty
    private Prefix prefix;

    @JsonProperty
    private CodeOffset code_offset;

    @JsonProperty
    private DataOffset data_offset;

    @JsonProperty
    private Map<String, String> register_byte;

    @Override
    public String toString() {
        return "Encoding{" +
                "REX=" + REX +
                ", opcode=" + opcode +
                ", ModRM=" + ModRM +
                ", VEX=" + VEX +
                ", EVEX=" + EVEX +
                ", immediate=" + immediate +
                ", prefix=" + prefix +
                ", code_offset=" + code_offset +
                ", data_offset=" + data_offset +
                '}';
    }
}
