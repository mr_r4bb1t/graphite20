package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.evex_serialization;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EVEXPrefix {
    @JsonProperty
    private String mm;

    @JsonProperty
    private String pp;

    /*
     * 0 or 1 or none
     * */
    @JsonProperty
    private String W;

    @JsonProperty
    private String LL;

    @JsonProperty
    private String RR;

    @JsonProperty("B")
    private String B;

    @JsonProperty
    private String X;

    @JsonProperty
    private String vvvv;

    @JsonProperty
    private String V;

    @JsonProperty("b")
    private String smallB;

    @JsonProperty
    private String aaa;

    @JsonProperty
    private String z;

    @JsonProperty
    private Integer disp8xN;

    @Override
    public String toString() {
        return "EVEXPrefix{" +
                "mm='" + mm + '\'' +
                ", pp='" + pp + '\'' +
                ", W='" + W + '\'' +
                ", LL='" + LL + '\'' +
                ", RR='" + RR + '\'' +
                ", B='" + B + '\'' +
                ", X='" + X + '\'' +
                ", vvvv='" + vvvv + '\'' +
                ", V='" + V + '\'' +
                ", smallB='" + smallB + '\'' +
                ", aaa='" + aaa + '\'' +
                ", z='" + z + '\'' +
                ", disp8xN=" + disp8xN +
                '}';
    }
}
