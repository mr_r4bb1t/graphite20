package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.form_serialization;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.encoding_serialization.Encoding;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.isa_serialization.ISA;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.operand_serialization.implicit_operands_serialization.ImplicitOperand;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.operand_serialization.Operand;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Form {

    @JsonProperty
    private String canceling_inputs;

    @JsonProperty
    private String xmm_mode;

    @JsonProperty
    private String mmx_mode;

    @JsonProperty
    private List<ISA> isa;

    @JsonProperty
    private List<Operand> operands;

    @JsonProperty
    private List<ImplicitOperand> implicit_operands;

    @JsonProperty
    private List<Encoding> encodings;

    @Override
    public String toString() {
        return "Form{" +
                "operands=" + operands +
                ", encodings=" + encodings +
                '}';
    }
}
