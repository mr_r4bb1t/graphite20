package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.immediate_serializtion;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Immediate {

    @JsonProperty
    private Integer size;

    @JsonProperty
    private String value;

    @Override
    public String toString() {
        return "Immediate{" +
                "size=" + size +
                ", value='" + value + '\'' +
                '}';
    }
}
