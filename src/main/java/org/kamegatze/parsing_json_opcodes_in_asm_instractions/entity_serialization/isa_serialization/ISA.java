package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.isa_serialization;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ISA {

    @JsonProperty
    private String id;

}
