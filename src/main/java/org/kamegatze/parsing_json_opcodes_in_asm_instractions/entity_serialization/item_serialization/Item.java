package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.item_serialization;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.form_serialization.Form;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.operand_serialization.Operand;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Item {
    @JsonProperty
    private String summary;

    @JsonProperty
    private List<Form> forms;

    @Override
    public String toString() {
        return "Item{" +
                "summary='" + summary + '\'' +
                ", forms=" + forms +
                '}';
    }
}