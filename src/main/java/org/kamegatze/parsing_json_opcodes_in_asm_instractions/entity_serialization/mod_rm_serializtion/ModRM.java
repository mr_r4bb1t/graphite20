package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.mod_rm_serializtion;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ModRM {
    @JsonProperty
    private String mode;

    @JsonProperty
    private String rm;

    @JsonProperty
    private String reg;

    @Override
    public String toString() {
        return "ModRM{" +
                "mode='" + mode + '\'' +
                ", rm='" + rm + '\'' +
                ", reg='" + reg + '\'' +
                '}';
    }
}
