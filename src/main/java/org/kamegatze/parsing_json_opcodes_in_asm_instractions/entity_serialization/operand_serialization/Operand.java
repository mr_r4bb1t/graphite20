package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.operand_serialization;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Operand extends Operands {

    @JsonProperty
    private String type;

    @JsonProperty
    private Boolean input;

    @JsonProperty
    private Boolean output;

    @JsonProperty
    private Integer extended_size;

    @Override
    public String toString() {
        return "Operand{" +
                "type='" + type + '\'' +
                ", input=" + input +
                ", output=" + output +
                ", extended_size=" + extended_size +
                '}';
    }
}
