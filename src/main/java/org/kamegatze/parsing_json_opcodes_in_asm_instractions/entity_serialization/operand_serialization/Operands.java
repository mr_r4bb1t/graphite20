package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.operand_serialization;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public abstract class Operands {

    private String  id;

    private String type;

    private Boolean input;

    private Boolean output;

    private Integer extended_size;
}
