package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.operand_serialization.implicit_operands_serialization;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.operand_serialization.Operands;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ImplicitOperand extends Operands {

    @JsonProperty
    private String  id;

    @JsonProperty
    private Boolean input;

    @JsonProperty
    private Boolean output;
}
