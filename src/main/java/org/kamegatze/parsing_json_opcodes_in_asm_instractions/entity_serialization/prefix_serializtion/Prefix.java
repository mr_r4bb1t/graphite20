package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.prefix_serializtion;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Prefix {
    @JsonProperty
    private Boolean mandatory;

    @JsonProperty
    private String bytes;

    @Override
    public String toString() {
        return "Prefix{" +
                "mandatory=" + mandatory +
                ", bytes='" + bytes + '\'' +
                '}';
    }
}
