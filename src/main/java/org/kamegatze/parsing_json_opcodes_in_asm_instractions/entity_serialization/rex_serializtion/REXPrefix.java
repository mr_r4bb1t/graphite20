package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.rex_serializtion;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class REXPrefix {
    @JsonProperty
    private String W;

    @JsonProperty
    private String R;

    @JsonProperty
    private String B;

    @JsonProperty
    private String X;

    @JsonProperty
    private Boolean mandatory;

    @Override
    public String toString() {
        return "REXPrefix{" +
                "W='" + W + '\'' +
                ", R='" + R + '\'' +
                ", B='" + B + '\'' +
                ", X='" + X + '\'' +
                ", mandatory=" + mandatory +
                '}';
    }
}
