package org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.vex_serializtion;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VEXPrefix {

    @JsonProperty
    private String type;

    @JsonProperty
    private String W;

    @JsonProperty
    private String R;

    @JsonProperty
    private String X;

    @JsonProperty
    private String B;

    /*
     * field mmmmm -> map_select read in https://wiki.osdev.org/X86-64_Instruction_Encoding#VEX.2FXOP_opcodes
     * */
    @JsonProperty("mmmmm")
    private String mapSelect;

    @JsonProperty
    private String vvvv;

    @JsonProperty
    private String L;

    @JsonProperty
    private String pp;

    @Override
    public String toString() {
        return "VEXPrefix{" +
                "type='" + type + '\'' +
                ", R='" + R + '\'' +
                ", X='" + X + '\'' +
                ", B='" + B + '\'' +
                ", mapSelect='" + mapSelect + '\'' +
                ", vvvv='" + vvvv + '\'' +
                ", L='" + L + '\'' +
                ", pp='" + pp + '\'' +
                '}';
    }
}
