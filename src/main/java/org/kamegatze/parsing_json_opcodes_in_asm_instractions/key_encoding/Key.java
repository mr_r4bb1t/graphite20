package org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding;

public interface Key {

    Key builderKey(Object object);
}
