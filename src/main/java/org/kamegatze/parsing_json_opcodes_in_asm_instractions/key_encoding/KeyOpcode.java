package org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding;

import lombok.*;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.encoding_serialization.Encoding;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.vex.EnumVEX;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.vex.VEXPrefix;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.evex.EVEXPrefix;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.immediate.Immediate;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.mod_rm.ModRM;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.prefix.Prefix;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.rex.REXPrefix;

import java.math.BigInteger;
import java.util.HexFormat;

@Data
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class KeyOpcode implements Key {

    private BigInteger opcode;

    private Prefix prefix;

    private Immediate immediate;

    private REXPrefix rexPrefix;

    private ModRM modRM;

    private VEXPrefix vexPrefix;

    private EVEXPrefix evexPrefix;

    private byte[] decodeHexString(String hexString) {
        return HexFormat.of().parseHex(hexString);

    }

    @Override
    public Key builderKey(Object object) {
        Encoding encoding = (Encoding) object;

        return KeyOpcode.builder()
                .opcode( encoding.getOpcode().get("bytes") == null ? null :
                        new BigInteger(
                                decodeHexString(encoding.getOpcode()
                                        .get("bytes"))
                        )
                )
                .prefix(encoding.getPrefix() == null ? null : Prefix.builder()
                        /*
                         * correction value on null
                         * */
                        .bytes( encoding.getPrefix().getBytes() == null ? null :
                                new BigInteger(
                                        decodeHexString(encoding.getPrefix()
                                                .getBytes())
                                )
                        )
                        .mandatory(encoding.getPrefix().getMandatory() == null ? null :
                                encoding.getPrefix().getMandatory()
                        )
                        .build()
                )
                .modRM(encoding.getModRM() == null ? null : ModRM.builder()
                        .mode(encoding.getModRM().getMode() == null ? null :
                                encoding.getModRM().getMode())
                        .reg(encoding.getModRM().getReg() == null ? null :
                                encoding.getModRM().getReg())
                        .rm(encoding.getModRM().getRm() == null ? null :
                                encoding.getModRM().getRm())
                        .build()
                )
                .immediate(encoding.getImmediate() == null ? null : Immediate.builder()
                        .size(encoding.getImmediate().getSize() == null ? null :
                                encoding.getImmediate().getSize())
                        .value(encoding.getImmediate().getValue() == null ? null :
                                encoding.getImmediate().getValue())
                        .build()
                )
                .evexPrefix(encoding.getEVEX() == null ? null : EVEXPrefix.builder()
                        .aaa(encoding.getEVEX().getAaa() == null ? null :
                                encoding.getEVEX().getAaa())
                        .B(encoding.getEVEX().getB() == null ? null :
                                encoding.getEVEX().getB())
                        .smallB(encoding.getEVEX().getSmallB() == null ? null :
                                encoding.getEVEX().getSmallB())
                        .W(encoding.getEVEX().getW() == null ?
                                null : Integer.valueOf(encoding.getEVEX().getW()))
                        .V(encoding.getEVEX().getV() == null ? null :
                                encoding.getEVEX().getV())
                        .X(encoding.getEVEX().getX() == null ? null :
                                encoding.getEVEX().getX())
                        .z(encoding.getEVEX().getZ() == null ? null :
                                encoding.getEVEX().getZ())
                        .mm(encoding.getEVEX().getMm() == null ? null :
                                encoding.getEVEX().getMm())
                        .pp(encoding.getEVEX().getPp() == null ? null :
                                encoding.getEVEX().getPp())
                        .RR(encoding.getEVEX().getRR() == null ? null :
                                encoding.getEVEX().getRR())
                        .LL(encoding.getEVEX().getLL() == null ? null :
                                encoding.getEVEX().getLL())
                        .vvvv(encoding.getEVEX().getVvvv() == null ? null :
                                encoding.getEVEX().getVvvv())
                        .disp8xN(encoding.getEVEX().getDisp8xN() == null ? null :
                                encoding.getEVEX().getDisp8xN())
                        .build()
                )
                .rexPrefix(encoding.getREX() == null ? null : REXPrefix.builder()
                        .B(encoding.getREX().getB())
                        .R(encoding.getREX().getR())
                        .X(encoding.getREX().getX())
                        /*
                         * correction value on null
                         * */
                        .W(new BigInteger(
                                        encoding.getREX().getW()
                                )
                        )
                        .mandatory(encoding.getREX().getMandatory())
                        .build()
                )
                .vexPrefix(encoding.getVEX() == null ? null : VEXPrefix.builder()
                        .B(encoding.getVEX().getB() == null ? null :
                                encoding.getVEX().getB())
                        /*
                         * correction value on null
                         * */
                        .L(encoding.getVEX().getL() == null ? null :
                                new BigInteger(encoding.getVEX().getL())
                        )
                        .X(encoding.getVEX().getX() == null ? null :
                                encoding.getVEX().getX())
                        .R(encoding.getVEX().getR() == null ? null :
                                encoding.getVEX().getR())
                        .vvvv(encoding.getVEX().getVvvv() == null ? null :
                                encoding.getVEX().getVvvv())
                        .pp(encoding.getVEX().getPp() == null ? null :
                                encoding.getVEX().getPp())
                        .type(encoding.getVEX().getType() == null ? null :
                                EnumVEX.valueOf(encoding.getVEX().getType()))
                        .mapSelect(encoding.getVEX().getMapSelect() == null ? null :
                                encoding.getVEX().getMapSelect())
                        .build()
                )
                .build();
    }
}
