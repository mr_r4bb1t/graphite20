package org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.evex;

import lombok.*;

import java.math.BigInteger;

@Data
@Builder
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class EVEXPrefix {

    private String mm;

    private String pp;

    /*
    * 0 or 1 or none
    * */
    private Integer W;

    private String LL;

    private String RR;

    private String B;

    private String X;

    private String vvvv;

    private String V;

    private String smallB;

    private String aaa;

    private String z;

    private Integer disp8xN;

}
