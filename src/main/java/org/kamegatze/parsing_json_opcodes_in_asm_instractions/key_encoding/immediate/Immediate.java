package org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.immediate;

import lombok.*;

@Data
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class Immediate {

    private Integer size;

    private String value;

}
