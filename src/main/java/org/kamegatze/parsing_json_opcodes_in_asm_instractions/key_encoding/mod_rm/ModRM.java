package org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.mod_rm;

import lombok.*;

@Data
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class ModRM {
    private String mode;

    private String rm;

    private String reg;
}
