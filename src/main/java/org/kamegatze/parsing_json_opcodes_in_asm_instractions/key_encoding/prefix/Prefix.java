package org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.prefix;

import lombok.*;

import java.math.BigInteger;

@Data
@ToString
@EqualsAndHashCode
@Builder
@AllArgsConstructor
public class Prefix {

    private Boolean mandatory;

    private BigInteger bytes;

}
