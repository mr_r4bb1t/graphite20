package org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.rex;

import lombok.*;

import java.math.BigInteger;

@Data
@EqualsAndHashCode
@ToString
@Builder
@AllArgsConstructor
public class REXPrefix {

    private BigInteger W;

    private String R;

    private String B;

    private String X;

    private Boolean mandatory;
}