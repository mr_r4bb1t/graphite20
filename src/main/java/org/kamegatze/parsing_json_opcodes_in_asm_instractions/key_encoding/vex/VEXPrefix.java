package org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.vex;

import lombok.*;

import java.math.BigInteger;

@Data
@Builder
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class VEXPrefix {

    private EnumVEX type;

    private String R;

    private String X;

    private String B;

    /*
     * field mmmmm -> map_select read in https://wiki.osdev.org/X86-64_Instruction_Encoding#VEX.2FXOP_opcodes
     * */
    private String mapSelect;

    private String vvvv;

    private BigInteger L;

    private String pp;

}
