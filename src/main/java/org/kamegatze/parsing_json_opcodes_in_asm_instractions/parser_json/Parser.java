package org.kamegatze.parsing_json_opcodes_in_asm_instractions.parser_json;

import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.RootJson;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.Key;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface Parser {
     RootJson getRootJson() throws IOException;

     Key builderKey(Object object);

     Map<Key, Map<String, List<Object>>> transformation(RootJson rooJson);
}
