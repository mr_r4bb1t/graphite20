package org.kamegatze.parsing_json_opcodes_in_asm_instractions.parser_json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.Instructions;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.RootJson;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.encoding_serialization.Encoding;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.form_serialization.Form;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.item_serialization.Item;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.entity_serialization.operand_serialization.implicit_operands_serialization.ImplicitOperand;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.Key;
import org.kamegatze.parsing_json_opcodes_in_asm_instractions.key_encoding.KeyOpcode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParserInstructions implements Parser {

    private final RootJson rootJson = Instructions.builder().build();

    private final Key key = KeyOpcode.builder().build();

    private final ObjectMapper objectMapper = new ObjectMapper();

    public ParserInstructions() throws IOException {
    }

    @Override
    public RootJson getRootJson() throws IOException {
        return rootJson.getRooJson(objectMapper);
    }

    @Override
    public Key builderKey(Object object) {
        return key.builderKey(object);
    }

    /**
     * {
     *      Key : {
     *          One element
     *          "asmInstruction" : ["String"],
     *          List elements
     *          "operands": [...Operands]
     *      }
     * }
     * */
    @Override
    public Map<Key, Map<String, List<Object>>> transformation(RootJson rooJson) {
        Instructions instructions = (Instructions) rooJson;

        Map<String, Item> jsonFile = instructions.getInstructions();

        Map<Key, Map<String, List<Object>>> asmInstruction = new HashMap<>();

        for (String key : jsonFile.keySet()) {

            for(Form form : jsonFile.get(key).getForms()) {

                for (Encoding encoding : form.getEncodings()) {
                    /*
                     * forming key for opcode
                     * */
                    Key keyOpcode = builderKey(encoding);

                    Map<String, List<Object>> asmItem = new HashMap<>();

                    asmItem.put("asmInstruction", List.of(key));

                    List<Object> operands = new ArrayList<>();

                    if(form.getOperands() != null) {
                        operands.addAll(form.getOperands());
                    }

                    if(form.getImplicit_operands() != null) {
                        for(ImplicitOperand implicitOperand : form.getImplicit_operands()) {
                            implicitOperand.setType(implicitOperand.getId());
                            implicitOperand.setId(null);
                        }
                        operands.addAll(form.getImplicit_operands());
                    }

                    asmItem.put("operands", operands);

                    asmInstruction.put(keyOpcode, asmItem);
                }
            }
        }

        return asmInstruction;
    }
}
